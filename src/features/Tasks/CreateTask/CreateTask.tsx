import React from "react";
import {Button} from "@material-ui/core";
import Modal from "../../../components/Layout/Modal/Modal";


const CreateTask = () => {
    const [open, setOpen] = React.useState<boolean>(false);

    const handleClose = () => setOpen(!open);
    const handleSubmit = () => {
        alert("TODO: add task form")
    }

    return (
        <div>
            <Button onClick={() => setOpen(true)}>Add Task</Button>
            <Modal
                title={"Create a new task"}
                open={open}
                onAgree={handleSubmit}
                onDisagree={handleClose}
                onClose={handleClose}
            />

        </div>
    );
}

export default CreateTask;