import React from 'react';
import {Button, Dialog, DialogActions, DialogContent, DialogTitle, Slide} from "@material-ui/core";
import {TransitionProps} from "@material-ui/core/transitions";

interface IProps {
    title: string
    open: boolean
    children?: React.ReactNode

    onClose(): void
    onAgree(): void
    onDisagree(): void
}

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & { children?: React.ReactElement<any, any> },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const Modal = ({title, open, children, onClose, onAgree, onDisagree}: IProps) => {

    return (
        <Dialog
            open={open}
            onClose={onClose}
            TransitionComponent={Transition}
            keepMounted
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >

            <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
            <DialogContent>{children}</DialogContent>
            <DialogActions>
                <Button onClick={onDisagree} color="primary">
                    Disagree
                </Button>
                <Button onClick={onAgree} color="primary" autoFocus>
                    Agree
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default Modal;