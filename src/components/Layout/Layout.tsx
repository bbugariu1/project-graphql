import React from "react";
import DefaultLayout from "./DefaultLayout/DefaultLayout";
import {Box, Container} from "@material-ui/core";
import {Switch, Route} from 'react-router';
import Login from "../../features/Login/Login";
import Tasks from "../../features/Tasks/Tasks";

const Layout = () => {

    return (
        <DefaultLayout>
            <Container maxWidth="lg">
                <Box my={4}>
                    <Switch>
                        <Route exact path="/" component={Tasks}/>
                        <Route exact path="/login" component={Login}/>
                    </Switch>
                </Box>
            </Container>
        </DefaultLayout>
    );
};

export default Layout;