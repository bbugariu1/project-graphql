import React from "react";
import {
    createStyles,
    Divider, Drawer,
    IconButton,
    List,
    ListItem, ListItemIcon, ListItemText,
    Theme,
    useTheme
} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import Header from "../DefaultLayout/Header/Header";
import clsx from "clsx";
import {Link} from "react-router-dom";
import ListItemLink from "../ListItemLink/ListItemLink";

const drawerWidth = 240;
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
        },
        drawer: {
            width: drawerWidth,
            flexShrink: 0,
        },
        drawerPaper: {
            width: drawerWidth,
        },
        drawerHeader: {
            display: 'flex',
            alignItems: 'center',
            padding: theme.spacing(0, 1),
            // necessary for content to be below app bar
            ...theme.mixins.toolbar,
            justifyContent: 'flex-end',
        },
        content: {
            flexGrow: 1,
            marginTop: theme.spacing(3),
            padding: theme.spacing(3),
            transition: theme.transitions.create('margin', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.leavingScreen,
            }),
            marginLeft: -drawerWidth,
        },
        contentShift: {
            transition: theme.transitions.create('margin', {
                easing: theme.transitions.easing.easeOut,
                duration: theme.transitions.duration.enteringScreen,
            }),
            marginLeft: 0,
        },
    }),
);

interface IProps {
    children: React.ReactNode;
}

const AppDrawer = (props: IProps) => {
    const classes = useStyles();
    const theme = useTheme();
    const [open, setOpen] = React.useState<boolean>(false);

    const handleDrawerOpen = () => setOpen(true);
    const handleDrawerClose = () => setOpen(false);

    return (
        <div className={classes.root}>
            <Header handleDrawerOpen={handleDrawerOpen} open={open}/>

            <Drawer
                className={classes.drawer}
                variant="persistent"
                anchor="left"
                open={open}
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <div className={classes.drawerHeader}>
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'ltr' ? <ChevronLeftIcon/> : <ChevronRightIcon/>}
                    </IconButton>
                </div>
                <Divider/>
                <List>
                    <ListItemLink text={'tasks'} icon={<InboxIcon/>} to={'tasks'} />
                </List>
            </Drawer>
            <main className={clsx(classes.content, {[classes.contentShift]: open,})}>
                <div>{props.children}</div>
            </main>
        </div>
    );
}


export default AppDrawer;