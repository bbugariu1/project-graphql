import React from "react";
import {Box, Container, Theme} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import AppDrawer from "../AppDrawer/AppDrawer";

interface IProps {
    children: React.ReactNode
}


const DefaultLayout = (props: IProps) => {
    return (
        <>
            <AppDrawer>
                {props.children}
            </AppDrawer>
        </>
    );
};

export default DefaultLayout;