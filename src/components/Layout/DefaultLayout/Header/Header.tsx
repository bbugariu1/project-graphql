import React from "react";
import clsx from "clsx";
import {AppBar, IconButton, Theme, Toolbar, Typography} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import {makeStyles} from "@material-ui/core/styles";

interface IProps {
    handleDrawerOpen () : void
    open: boolean
}

const drawerWidth = 240;
const useStyles = makeStyles((theme: Theme) => ({
    appBar: {
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    hide: {
        display: 'none',
    },
}))

const Header = (props: IProps) => {
    const classes = useStyles();
    return (
        <AppBar position="fixed" className={clsx(classes.appBar, {[classes.appBarShift]: props.open,})}>
            <Toolbar>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={props.handleDrawerOpen}
                    edge="start"
                    className={clsx(classes.menuButton, props.open && classes.hide)}
                >
                    <MenuIcon />
                </IconButton>
                <Typography variant="h6" noWrap>
                    GraphQl Project
                </Typography>
            </Toolbar>
        </AppBar>
    );
}

export default Header;