import React from "react";
import {Link, LinkProps} from "react-router-dom";
import {ListItem, ListItemIcon, ListItemText} from "@material-ui/core";

interface IProps {
    icon: any;
    text: string;
    primary?: React.ReactNode;
    to: string;
}

const ListItemLink = (props: IProps) => {
    const CustomLink = React.useMemo(
        () =>
            React.forwardRef((linkProps, ref: any) => (
                <Link ref={ref} to={props.to} {...linkProps} />
            )),
        [props.to],
    );

    return (
        <li>
            <ListItem button component={CustomLink}>
                <ListItemIcon>{props.icon}</ListItemIcon>
                <ListItemText primary={props.primary} />
                <ListItemText primary={"Tasks"}/>
            </ListItem>
        </li>
    );
}

export default ListItemLink;