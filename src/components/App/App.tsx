import React from 'react';
import {gql, useQuery} from "@apollo/client";
import Layout from "../Layout/Layout";
import {CssBaseline} from "@material-ui/core";

const USER_QUERY = gql`
    query {
        users {
            id
            name
            email
        }
    }
`;

function App() {

    // const {loading, error, data} = useQuery(USER_QUERY);
    //
    // if (loading) {
    //     return <p>Loading ...</p>
    // }
    //
    // if (error) {
    //     return <p>{error.message}</p>
    // }
    //
    // const renderUsers = () => {
    //     if (data) {
    //         return data.users.map((user: any) => {
    //             return <li key={user.id}>{user.name}</li>
    //         })
    //     }
    //
    //     return 1;
    // }

    return (
        <>
            <CssBaseline />
            <Layout />
        </>
    )
}

export default App;
